package ru.mipt;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.reduce.IntSumReducer;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import java.lang.InterruptedException;
import java.io.IOException;
import org.apache.hadoop.mapreduce.lib.partition.InputSampler;
import org.apache.hadoop.mapreduce.lib.partition.TotalOrderPartitioner;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FSDataInputStream;
//import org.apache.commons.io.FileUtils;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import java.io.*;
import java.lang.*;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ActiveUserCount extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        ToolRunner.run(new ActiveUserCount(), args);
    }
    private static final int numReduceTasks = 8;

    @Override
    public int run(String[] args) throws Exception {
        boolean local = false;
        String time = getCurrentDateTime();
        Configuration conf = this.getConf();

        Job questionsJob = new Job(conf);
        questionsJob.setJarByClass(ActiveUserCount.class);
        questionsJob.setMapperClass(QuestionsMapper.class);
        questionsJob.setReducerClass(QuestionsReducer.class);
        questionsJob.setInputFormatClass(TextInputFormat.class);
        questionsJob.setOutputFormatClass(TextOutputFormat.class);
        questionsJob.setMapOutputKeyClass(LongWritable.class);
        questionsJob.setMapOutputValueClass(NullWritable.class);
        questionsJob.setNumReduceTasks(numReduceTasks);
        FileInputFormat.addInputPath(questionsJob, new Path(args[0]));
        FileOutputFormat.setOutputPath(questionsJob, new Path(args[1] + "-" + time + "/question_user_id"));
        if (!questionsJob.waitForCompletion(true)) {
            return -1;
        }

        Job answerJob = new Job(conf);
        answerJob.setJarByClass(ActiveUserCount.class);
        answerJob.setMapperClass(AnswerMapper.class);
        answerJob.setReducerClass(AnswerReducer.class);
        answerJob.setInputFormatClass(TextInputFormat.class);
        answerJob.setOutputFormatClass(TextOutputFormat.class);
        answerJob.setMapOutputKeyClass(LongWritable.class);
        answerJob.setMapOutputValueClass(Text.class);
        answerJob.setNumReduceTasks(numReduceTasks);
        FileInputFormat.addInputPath(answerJob, new Path(args[0]));
        FileOutputFormat.setOutputPath(answerJob, new Path(args[1] + "-" + time + "/answer_user_id"));
        if (!answerJob.waitForCompletion(true)) {
            return -1;
        }

        Job collectJob = new Job(conf);
        collectJob.setJarByClass(ActiveUserCount.class);
        collectJob.setMapperClass(CollectMapper.class);
        collectJob.setReducerClass(CollectReducer.class);
        collectJob.setInputFormatClass(TextInputFormat.class);
        collectJob.setOutputFormatClass(TextOutputFormat.class);
        collectJob.setMapOutputKeyClass(LongWritable.class);
        collectJob.setMapOutputValueClass(LongWritable.class);
        collectJob.setNumReduceTasks(numReduceTasks);
        MultipleInputs.addInputPath(collectJob,
                                    new Path(args[1] + "-" + time + "/answer_user_id"),
                                    TextInputFormat.class,
                                    CollectMapper.class);
        MultipleInputs.addInputPath(collectJob,
                                    new Path(args[1] + "-" + time + "/question_user_id"),
                                    TextInputFormat.class,
                                    CollectMapper.class);
        FileOutputFormat.setOutputPath(collectJob, new Path(args[1] + "-" + time + "/res"));
        if (!collectJob.waitForCompletion(true)) {
            return -1;
        }
        System.out.println(collectJob.getCounters().findCounter("Global", "UserNumber").getValue());
        return 0;
    }

    /*public void Clean(String outputPath) {
        FileUtils.deleteDirectory(new File(outputPath + "/tmp"));
        FileUtils.deleteQuietly(new File(outputPath + "/partitioning.lst"));
    }*/

    public static int getFieldByName(String line, String fieldName) {
        Pattern p = Pattern.compile(" " + fieldName + "=\"[0-9]+\"");
        Matcher m = p.matcher(line);
        String value = "";
        if (m.find()) {
            value = line.substring(m.start() + fieldName.length() + 3, m.end() - 1);
            return Integer.parseInt(value);
        } else {
            return -1;
        }
    }

    public static class QuestionsMapper extends Mapper<LongWritable, Text, LongWritable, NullWritable> {

        private LongWritable res = new LongWritable();
        @Override
        public void map(LongWritable offset, Text line, Context context)
            throws IOException, InterruptedException {
            if (getFieldByName(line.toString(), "PostTypeId") == 1 &&
                getFieldByName(line.toString(), "FavoriteCount") >= 100) {
                res.set(getFieldByName(line.toString(), "OwnerUserId"));
                context.write(res, NullWritable.get());
            };
        }
    }

    public static class QuestionsReducer extends Reducer<LongWritable, NullWritable, LongWritable, LongWritable> {

        private static LongWritable one = new LongWritable(1);
        @Override
        protected void reduce(LongWritable id, Iterable<NullWritable> values, Context context)
            throws IOException,InterruptedException {
            for (NullWritable value : values) {
                context.write(id, one);
                break;
            }
       }
    }

    public static class AnswerMapper extends Mapper<LongWritable, Text, LongWritable, Text> {

        @Override
        public void map(LongWritable offset, Text line, Context context)
                throws IOException, InterruptedException {
            if (getFieldByName(line.toString(), "PostTypeId") == 2) {
                Integer questionId = getFieldByName(line.toString(), "ParentId");
                Integer answerUserId = getFieldByName(line.toString(), "OwnerUserId");
                Integer score = getFieldByName(line.toString(), "Score");
                context.write(new LongWritable(getFieldByName(line.toString(), "ParentId")),
                              new Text(answerUserId.toString() + '\t' + score.toString()));
            };
        }
    }

    public static class AnswerReducer extends Reducer<LongWritable, Text, LongWritable, LongWritable> {

        private static LongWritable zero = new LongWritable(0);
        @Override
        protected void reduce(LongWritable id, Iterable<Text> values, Context context)
                throws IOException,InterruptedException {
            int bestScore = -2000000000;
            int bestUserId = -1;
            for (Text value : values) {
                int score = Integer.parseInt(value.toString().split("\t")[1]);
                int userId = Integer.parseInt(value.toString().split("\t")[0]);
                if (score > bestScore) {
                    bestUserId = userId;
                    bestScore = score;
                }
            }
            if (bestUserId != -1) {
                context.write(new LongWritable(bestUserId), zero);
            }
        }
    }

    public static class CollectMapper extends Mapper<LongWritable, Text, LongWritable, LongWritable> {

        @Override
        public void map(LongWritable offset, Text line, Context context)
                throws IOException, InterruptedException {
            String [] numbers = line.toString().split("\t");
            context.write(new LongWritable(Integer.parseInt(numbers[0])),
                          new LongWritable(Integer.parseInt(numbers[1])));
        }
    }

    public static class CollectReducer extends Reducer<LongWritable, LongWritable, LongWritable, NullWritable> {

        @Override
        protected void reduce(LongWritable id, Iterable<LongWritable> values, Context context)
                throws IOException,InterruptedException {
            boolean hasQuestion = false;
            boolean hasAnswer = false;
            for (LongWritable value : values) {
                if (value.get() == 0) {
                    hasAnswer = true;
                }
                if (value.get() == 1) {
                    hasQuestion = true;
                }
                if (hasQuestion && hasAnswer) {
                    context.write(id, NullWritable.get());
                    context.getCounter("Global", "UserNumber").increment(1);
                    return;
                }
            }
        }
    }

    private static String getCurrentDateTime() {
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        return sdf.format(d);
    }
}
