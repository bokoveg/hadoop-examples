package ru.mipt;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.reduce.IntSumReducer;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import java.lang.InterruptedException;
import java.io.IOException;
import org.apache.hadoop.mapreduce.lib.partition.InputSampler;
import org.apache.hadoop.mapreduce.lib.partition.TotalOrderPartitioner;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FSDataInputStream;
//import org.apache.commons.io.FileUtils;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import java.io.*;
import java.lang.*;


public class WordCount extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        ToolRunner.run(new WordCount(), args);
    }
    private static final int numReduceTasks = 8;

    @Override
    public int run(String[] args) throws Exception {
        boolean local = false;
        String time = getCurrentDateTime();
        Configuration conf = this.getConf();
        String s = "";
        for (int i = 0; i < Math.min(Integer.parseInt(args[2]), 9); i++) {
            s += "9";
        }
        if (args.length == 4 && args[3].equals("local")) {
            local = true;
        }
        conf.set("maxvalue", s);
        // first MapReduce job - counting words
        Job wordCountJob = new Job(conf);
        wordCountJob.setJarByClass(WordCount.class);
        wordCountJob.setMapperClass(WordCountMapper.class);
        wordCountJob.setReducerClass(WordCountReducer.class);
        wordCountJob.setInputFormatClass(TextInputFormat.class);
        wordCountJob.setOutputFormatClass(TextOutputFormat.class);
        wordCountJob.setMapOutputKeyClass(Text.class);
        wordCountJob.setMapOutputValueClass(LongWritable.class);
        wordCountJob.setNumReduceTasks(numReduceTasks);

        FileInputFormat.addInputPath(wordCountJob, new Path(args[0]));
        FileOutputFormat.setOutputPath(wordCountJob, new Path(args[1] + time + "/tmp"));

        if (!wordCountJob.waitForCompletion(true)) {
            return -1;
        }

        // second MapReduce job - sorting
        Job sortJob = new Job(conf);
        sortJob.setJarByClass(WordCount.class);
        sortJob.setMapperClass(SortMapper.class);
        sortJob.setReducerClass(SortReducer.class);
        sortJob.setInputFormatClass(KeyValueTextInputFormat.class);
        sortJob.setOutputFormatClass(TextOutputFormat.class);
        sortJob.setMapOutputKeyClass(Text.class);
        sortJob.setMapOutputValueClass(NullWritable.class);
        sortJob.setPartitionerClass(TotalOrderPartitioner.class);
        sortJob.setNumReduceTasks(numReduceTasks);

        FileInputFormat.addInputPath(sortJob, new Path(args[1] + time + "/tmp"));
        FileOutputFormat.setOutputPath(sortJob, new Path(args[1] + time + "/res"));
        Path partitionFile = new Path(args[1] + time + "/part/partitioning.lst");


        TotalOrderPartitioner.setPartitionFile(sortJob.getConfiguration(), partitionFile);
        InputSampler.Sampler<Text, Text> sampler =
                new InputSampler.RandomSampler<Text, Text>(0.1, 2000, 30000);
        InputSampler.writePartitionFile(sortJob, sampler);
        if (!sortJob.waitForCompletion(true)) {
            return -1;
        }

        // removing tmp and partition file
        //CleanOutputDirectory(args[1] + time);

        // printing top-10 to stdout
        PrintTop(args[1] + time + "/res", local);
        return 0;
    }

    /*public void Clean(String outputPath) {
        FileUtils.deleteDirectory(new File(outputPath + "/tmp"));
        FileUtils.deleteQuietly(new File(outputPath + "/partitioning.lst"));
    }*/

    public void PrintTop(String outputPath, boolean local) throws FileNotFoundException, IOException {
        BufferedReader bufferedReader;
        if (local) {
            FileInputStream fstream = new FileInputStream(outputPath + "/part-r-00000");
            bufferedReader = new BufferedReader(new InputStreamReader(fstream));
        } else {
            Configuration conf = new Configuration();
            Path path = new Path("hdfs://virtual-master.atp-fivt.org:8020/" +
                    outputPath + "/part-r-00000");
            FileSystem fs = FileSystem.get(path.toUri(), conf);
            FSDataInputStream inputStream = fs.open(path);
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        }
        String line;
        int count = 0;
        while ((line = bufferedReader.readLine()) != null && count < 10) {
            System.out.println(line);
            count++;
        }
        bufferedReader.close();
    }

    public static class WordCountMapper extends Mapper<LongWritable, Text, Text, LongWritable> {
        private final static LongWritable one = new LongWritable(1);
        private final static LongWritable zero = new LongWritable(0);
        private Text word = new Text();
        private final static String [] forbidden = {",", "\\.", "!", "\\?", ";", ":", "\\(", "\\)", "\""};

        public static String [] SplitString(String s) {

            for (String symbol : forbidden) {
                s = s.replaceAll(symbol, "");
            }
            return s.split("\\s+");
        }

        public static boolean IsWord(String word) {
            if (word.length() <= 9 && word.length() >= 6) {
                for (int i = 1; i < word.length(); i++) {
                    if (word.charAt(i) > 'z' || word.charAt(i) < 'a') {
                        return false;
                    }
                }
                if (Character.isLetter(word.charAt(0))) {
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        }

        @Override
        public void map(LongWritable offset, Text line, Context context)
            throws IOException, InterruptedException {
            String [] words = SplitString(line.toString());
            for (String s : words) {
                if (IsWord(s)) {
                    if (s.charAt(0) <= 'Z' && s.charAt(0) >= 'A') {
                        word.set(s.toLowerCase());
                        context.write(word, one);
                    } else {
                        word.set(s);
                        context.write(word, zero);
                    }
                }
            }
        }
    }

    public static class WordCountReducer extends Reducer<Text, LongWritable, Text, NullWritable> {

        private static Text compositeKey = new Text();

        @Override
        protected void reduce(Text word, Iterable<LongWritable> values, Context context)
            throws IOException,InterruptedException {
            int sum = 0;
            int size = 0;
            for (LongWritable value: values) {
                sum += value.get();
                ++size;
            }

            if (size == sum && word.toString().length() > 0) {
                int maxvalue = Integer.parseInt(context.getConfiguration().get("maxvalue"));
                String compositeKeyString = Integer.toString(maxvalue - sum) + "." + word;
                compositeKey.set(compositeKeyString);
                context.write(compositeKey, NullWritable.get());
            }
       }
    }

    public static class SortMapper extends Mapper<Text, Text, Text, NullWritable> {

        private static NullWritable empty = NullWritable.get();

        @Override
        public void map(Text compositeKey, Text t, Context context)
                throws IOException, InterruptedException {
            context.write(compositeKey, empty);
        }
    }

    public static class SortReducer extends Reducer<Text, NullWritable, Text, LongWritable> {

        private final static Text word = new Text();
        private final static LongWritable count = new LongWritable();

        @Override
        protected void reduce(Text compositeKey, Iterable<NullWritable> values, Context context)
                throws IOException,InterruptedException {
            word.set(compositeKey.toString().split("\\.")[1]);
            int maxvalue = Integer.parseInt(context.getConfiguration().get("maxvalue"));
            count.set(maxvalue - Integer.parseInt(compositeKey.toString().split("\\.")[0]));
            context.write(word, count);
        }
    }

    private static String getCurrentDateTime() {
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        return sdf.format(d);
    }
}
